use std::fs::File;
use std::io::prelude::*;

fn crosses(a: (f64, f64), b: (f64, f64)) -> f64 {
    let (ax, ay) = a;
    let (bx, by) = b;
    let mut res = 0.;
    if (ax * bx).signum() < 0. {
        // crosses y axis
        let s = ax / (ax - bx);
        let ys = ay + (by - ay) * s;
        if ys < 0. {
            res += 1.;
        } else if ys > 0. {
            res += 10.;
        }
    }
    if (ay * by).signum() < 0. {
        // crosses x axis
        let s = ay / (ay - by);
        let xs = ax + (bx - ax) * s;
        if xs < 0. {
            res += 100.;
        } else if xs > 0. {
            res += 1000.;
        }
    }
    res
}

fn main() {
    let mut f = File::open("p102_triangles.txt").unwrap();
    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    let mut res = 0;
    for l in contents.lines() {
        let elements: Vec<f64> = l.split(',').map(|x| x.parse::<f64>().unwrap()).collect();
        let a = (elements[0], elements[1]);
        let b = (elements[2], elements[3]);
        let c = (elements[4], elements[5]);
        let cross = crosses(a, b) + crosses(b, c) + crosses(c, a);
        if cross == 1111. {
            res += 1;
        }
    }
    println!("{}", res);
}
